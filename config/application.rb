require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)
ENVied.require(*ENV['ENVIED_GROUPS'] || Rails.groups)

# Ensure all required ENV variables are present
ENVied.require(*Rails.groups)

module DemoAuthenticationService
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    # Prepend all log lines with the following tags.
    config.log_tags = [:request_id]
    config.log_level = ENVied.LOG_LEVEL

    # use a log formatter which both includes progname and correctly tags multi-line log events
    config.log_formatter = -> (severity, time, progname, msg) {
      line_prefix = severity[0, 1] << ' '
      line_prefix << "[#{progname}] " if progname.present?
      first_line, *lines = msg.split("\n")
      if (m = /\A(?:\[.+?\] )+/.match(first_line))
        line_prefix << m.to_s
        first_line = m.post_match
      end
      lines.inject(line_prefix + first_line << "\n") { |string, line|
        string << line_prefix << line << "\n"
      }
    }

    config.external_url = ENVied.EXTERNAL_URL

    config.action_mailer.perform_deliveries = ENVied.RAILS_ACTION_MAILER_DELIVERY_URL
    if config.action_mailer.perform_deliveries
      am_url = ENVied.RAILS_ACTION_MAILER_DELIVERY_URL

      case am_url.scheme
      when 'smtp'
        config.action_mailer.delivery_method = :smtp
        config.action_mailer.smtp_settings = {
          address: am_url.host,
          port: am_url.port,
          authentication: :login,
          user_name: am_url.user,
          password: am_url.password,
          enable_starttls_auto: true,
          # ignore TLS certificate validity outside of production
          openssl_verify_mode: ENVied.DEV_MODE ? OpenSSL::SSL::VERIFY_NONE : OpenSSL::SSL::VERIFY_PEER
        }

      when 'amazon-ses'
        require 'aws-sdk-rails'
        config.action_mailer.delivery_method = :aws_sdk
        # eagerly attempt to create an SES client, in order to validate that credentials are present
        Aws::SES::Client.new

      else
        fail "Don't know how to configure ActionMailer for RAILS_ACTION_MAILER_DELIVERY_URL=#{am_url.to_s.inspect}"
      end
    end

    config.action_mailer.default_url_options = {
      host: config.external_url.host,
      protocol: config.external_url.scheme,
    }

    config.action_mailer.default_options = {
      from: '"Mattbot" <mattridenour@gmail.com>',
      reply_to: '"Mattbot" <mattridenour@gmail.com>',
    }

    # only set port if non-standard for the URL scheme:
    case config.external_url.scheme
    when 'http'
      config.action_mailer.default_url_options[:port] = config.external_url.port if config.external_url.port != 80
    when 'https'
      config.action_mailer.default_url_options[:port] = config.external_url.port if config.external_url.port != 443
    else
      fail 'wat'
    end

    config.running_in_docker = File.exist?('/.dockerenv')

    ##
    # Set a proper layout for Devise emails.
    # config.to_prepare {
    #   Devise::Mailer.layout 'mailer'
    #   Devise::Mailer.helper :mailer
    # }

  end
end

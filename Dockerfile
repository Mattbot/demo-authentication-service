FROM quay.io/mattbot/base-ruby:latest

RUN apt-get update \
 && apt-get upgrade \
 && apt-get install \
      build-essential \
      libmysqlclient-dev \
      libcurl4-openssl-dev \
      nodejs

RUN mkdir -p /gems

WORKDIR /app/

# ENV \
#   BUNDLE_PATH=/gems \
#   BUNDLE_RETRY=3 \
#   BUNDLE_WITHOUT=local_tools

COPY Gemfile Gemfile.lock /app/

RUN BUNDLE_IGNORE_MESSAGES=true bundle install --jobs $(getconf _NPROCESSORS_ONLN)

# Provide *default* values for some of our configuration variables:
ENV \
  CI=false \
  DEV_MODE=false \
  LOG_LEVEL=info \
  RAILS_CACHING=false \
  RAILS_FORCE_SSL=false \
  RAILS_LOG_TO_STDOUT=true \
  RAILS_SERVE_STATIC_FILES=true \
  PATH=/app/bin:$PATH

ENV PATH="/app/bin:$PATH"

COPY ./app/ /app/app
COPY ./bin/ /app/bin
COPY ./config/ /app/config
COPY ./lib/ /app/lib
COPY ./vendor/ /app/vendor
COPY Rakefile /app/

# RUN /app/bin/rake --trace assets:precompile \
#  && rm -rf tmp/* log/*

COPY . /app/

ENTRYPOINT ["/sbin/tini", "-vvg", "--", "/app/docker/entrypoint.sh"]

EXPOSE 3000

STOPSIGNAL SIGINT

CMD ["puma", "--config", "config/puma.rb"]
